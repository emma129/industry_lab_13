package ictgradschool.industry.lab13.ex05;

import ictgradschool.Keyboard;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lelezhao on 5/6/17.
 */
public class UserCommand {

    PrimeFactorsTask userTask;
    long endTime;
    long startTime;
    long timeInMillis;



    public void start() {
        System.out.println();
        System.out.println("==============================");
        System.out.println("Please enter a number to start");

        long userKeybordInput = Long.parseLong(Keyboard.readInput());
        PrimeFactorsTask userTask = new PrimeFactorsTask(userKeybordInput);

        List<Long> primeFactors = new ArrayList<>();
       // System.out.println(startTime);

        Thread computationThread = new Thread(userTask);


        System.out.println();
        System.out.println("==============================");
        System.out.println("Waiting for result...");

        Thread abortThread = new Thread(new Runnable() {
            @Override
            public void run() {
                while (!Thread.currentThread().isInterrupted()) {
                    endTime = System.currentTimeMillis();
                    timeInMillis = endTime - startTime;
                    if (timeInMillis >= 1000) {

                        System.out.println("It takes long time to calculating the result");
                        System.out.println("Type any key to stop ");
                        if (Keyboard.readInput() !=""){
                            computationThread.interrupt();
                            System.exit(0);
                        }

//                System.out.println("Press any key to stop the thread");
//                String userType = Keyboard.readInput();
//                if ( userType!= ""){
//                    System.out.println("The thread is interrupted");
//                    computationThread.interrupt();
//                }

                    }

                }
            }
        });

        computationThread.start();
        startTime = System.currentTimeMillis();
        abortThread.start();


        try {
            computationThread.join();
            abortThread.interrupt();
            abortThread.join();
        }catch (InterruptedException e){
            e.printStackTrace();
        }


        try {
            primeFactors = userTask.getPrimeFactors();
            System.out.println();
            System.out.print("The prime factors are:");
            for (long element : primeFactors) {
                System.out.print(" " + element);
            }
        }  catch (IllegalStateException e) {
            e.printStackTrace();
        }





//        try {
//            primeFactors = userTask.getPrimeFactors();
//            System.out.println();
//            System.out.print("The prime factors are:");
//            for (long element : primeFactors) {
//                System.out.print(" " + element);
//            }
//        } catch (IllegalStateException e) {
//            e.printStackTrace();
//        }



    }

    public static void main(String[] args) {

        new UserCommand().start();

    }


}
