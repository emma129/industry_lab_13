package ictgradschool.industry.lab13.ex05;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yzhb363 on 3/05/2017.
 */
public class PrimeFactorsTask implements Runnable {
    private long N;
    private List<Long> listOfPrimeFactors = new ArrayList();
    private TaskState state;

    public PrimeFactorsTask(long n) {
        this.state = TaskState.Initialized;
        this.N = n;
    }

    public void run() {
        for (long factor = 2L; factor * factor <= this.N && !Thread.currentThread().isInterrupted(); ++factor) {
            while (this.N % factor == 0L) {
                this.listOfPrimeFactors.add(Long.valueOf(factor));
                this.N /= factor;
            }
        }

        this.state = TaskState.Completed;
    }

    public long n() {
        return ((Long) this.listOfPrimeFactors.get(this.listOfPrimeFactors.size() - 1)).longValue();
    }

    public List<Long> getPrimeFactors() throws IllegalStateException {
        if (this.state.equals(TaskState.Completed)) {
            return this.listOfPrimeFactors;
        } else {
            throw new IllegalStateException();
        }
    }

    public TaskState getState() {
        return this.state;
    }
}

//    public class Factors {
//
//        public static void main(String[] args) {
//
//            // command-line argument
//            long n = Long.parseLong(args[0]);
//
//            System.out.print("The prime factorization of " + n + " is: ");
//
//            // for each potential factor
//            for (long factor = 2; factor*factor <= n; factor++) {
//
//                // if factor is a factor of n, repeatedly divide it out
//                while (n % factor == 0) {
//                    System.out.print(factor + " ");
//                    n = n / factor;
//                }
//            }
//
//            // if biggest factor occurs only once, n > 1
//            if (n > 1) System.out.println(n);
//            else       System.out.println();
//        }
//    }



