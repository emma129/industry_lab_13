package ictgradschool.industry.lab13.ex04;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingDeque;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.RunnableFuture;

import static ictgradschool.industry.lab13.ex04.Transaction.TransactionType.Deposit;
import static ictgradschool.industry.lab13.ex04.Transaction.TransactionType.Withdraw;

/**
 * Created by yzhb363 on 2/05/2017.
 */
public class ConcurrentBankingApp {

    BlockingQueue<Transaction> newQueue = new ArrayBlockingQueue<Transaction>(10);
    BankAccount newAccount = new BankAccount();
    List<Transaction> transDetails = TransactionGenerator.readDataFile();
    List<Thread> twoConsumerThread = new ArrayList<>();


    /*    //Acquire Transactions to process.
        List<Transaction> transactions = TransactionGenerator.readDataFile();

        //Create BankAccount object to operate on.
        BankAccount account = new BankAccount();

         //For each Transaction, apply it to the BankAccount instance.
            for (Transaction transaction : transactions) {
            switch (transaction._type) {
                case Deposit:
                    account.deposit(transaction._amountInCents);
                    break;
                case Withdraw:
                    account.withdraw(transaction._amountInCents);
                    break;
            }
        }

        //Print the final balance after applying all Transactions.
            System.out.println("Final balance: " + account.getFormattedBalance());
        */
    public void start() {

        Thread producerThread = new Thread(new Runnable() {
            @Override
            public void run() {
                for (Transaction allDetails : transDetails) { //generate data and add to the buffer
                    try {
                        newQueue.put(allDetails);
                    } catch (InterruptedException e) {
                        System.out.println("cannot add the transaction");
                    }

                }
            }

        });

        for (int i = 0; i < 2; i++) {
            Thread consumerThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    try {
                        while (!Thread.currentThread().isInterrupted()) {
                            Transaction transaction = null;
                            transaction = newQueue.take();
                            if (transaction != null) {
                                switch (transaction._type) {
                                    case Deposit:
                                        newAccount.deposit(transaction._amountInCents);
                                        break;
                                    case Withdraw:
                                        newAccount.withdraw(transaction._amountInCents);
                                        break;
                                }
                            }
                        }
                    } catch (InterruptedException e) {
                        e.getCause();
                    }
                }

            });
            twoConsumerThread.add(consumerThread);
        }





        producerThread.start();
        for (Thread newThread : twoConsumerThread) {
            newThread.start();
        }



        try {
            producerThread.join();

            while (!newQueue.isEmpty());

            for (Thread newThread : twoConsumerThread) {
                newThread.interrupt();
            }


            for (Thread newThread : twoConsumerThread) {
                newThread.join();
            }


        } catch (InterruptedException e) {
            e.printStackTrace();
        }


        System.out.println("Final balance: " + newAccount.getFormattedBalance());

    }


    public static void main(String[] args) {

        new ConcurrentBankingApp().start();

    }

}
