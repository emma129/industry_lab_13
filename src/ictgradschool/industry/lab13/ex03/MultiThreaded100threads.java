package ictgradschool.industry.lab13.ex03;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;


/**
 * TODO Create a multi-threaded program to calculate PI using the Monte Carlo method.
 */
public class MultiThreaded100threads extends ExerciseThreeSingleThreaded {

    /**
     * Estimates PI using a multi-threaded Monte Carlo method.
     */
    double estimatedPi;
    @Override
    protected double estimatePI(long numSamples) {
        // TODO Implement this.
        int newNum =0;
        int newThreadTime = 100;
        long threadNum = numSamples /newThreadTime;
        List<Thread> threadCircle = new ArrayList<>();

        final long numInsideCircle[] = new long[newThreadTime] ;// each thread gets different numInsideCircle

        for (int i = 0; i < newThreadTime ; i++) {
            int finalI = i;// in line35 [] request int
            Thread newThread = new Thread(new Runnable() {
                @Override
                public void run() {
                    for (long j = 0; j < threadNum; j++) {
                        ThreadLocalRandom tlr = ThreadLocalRandom.current();
                        double x = tlr.nextDouble();
                        double y = tlr.nextDouble();
                        if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
                            numInsideCircle[finalI]++; //getting different numbers
                        }
                    }
                }

            });

            threadCircle.add(newThread);
            newThread.start();

        }


         for (Thread allThreads: threadCircle){
            try {
                allThreads.join();
            }catch (InterruptedException e){
                e.printStackTrace();
             }
         }

         for (int x=0; x<newThreadTime ; x++){

            newNum += numInsideCircle[x];

         }



            /**ThreadLocalRandom tlr = ThreadLocalRandom.current();

             long numInsideCircle = 0;

             for (long i = 0; i < numSamples; i++) {

             double x = tlr.nextDouble();
             double y = tlr.nextDouble();

             if (Math.pow(x, 2.0) + Math.pow(y, 2.0) < 1.0) {
             numInsideCircle++;
             }

             }

             double estimatedPi = 4.0 * (double) numInsideCircle / (double) numSamples;
             return  estimatedPi;**/


        return  4.0 * (double) (newNum) / (double) numSamples; //have two results of numInsideCircle
    }




    /** Program entry point. */
    public static void main(String[] args) {
        new MultiThreaded100threads().start();
    }
}
